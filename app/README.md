# Enext Front-End Test

### Description

This project was developed as a test for Enext Company.
To run the project your must have Node +12 installed, create-react-app and yarn.

### Dependencies

To install the dependencies your should enter in the */app/* folder and type in the command line to install :

```
yarn
```

### Running

To start the application just go to */app/* and type in the command line:

```
yarn start
```

Open [http://localhost:3000](http://localhost:3000) in the browser and the application should be running