import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';

import { theme } from './theme';
import store from './state/store';
import GlobalStyle from './styles/global';

import Home from './pages/Home';

const App = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Home />
        <GlobalStyle />
      </ThemeProvider>
    </Provider>
  );
}

export default App;
