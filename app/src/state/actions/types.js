export const GET_BREEDS = 'GET_BREEDS';
export const GET_RANDOM_IMAGE_BY_BREED = 'GET_RANDOM_IMAGE_BY_BREED';

export const ADD_PET = 'ADD_PET';
export const GET_PETS = 'GET_PETS';