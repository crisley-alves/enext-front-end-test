import axios from 'axios';
import { GET_BREEDS, GET_RANDOM_IMAGE_BY_BREED } from './types';

export const getBreeds = () => (dispatch) => {
  axios.get('https://dog.ceo/api/breeds/list/all')
    .then(response => {
      dispatch({
        type: GET_BREEDS,
        payload: response.data,
      });
    }).catch(error => console.error(error, 'request failed'));
};

export const getImageByBreed = (breed = 'cattledog/australian') => (dispatch) => {
  axios.get(`https://dog.ceo/api/breed/${breed}/images/random`)
    .then(response => {
      dispatch({
        type: GET_RANDOM_IMAGE_BY_BREED,
        payload: response.data,
      });
    }).catch(error => console.error(error, 'request failed'));
};
