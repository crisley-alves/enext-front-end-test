import { ADD_PET, GET_PETS } from './types';

export const addPet = (data) => (dispatch) => dispatch({ type: ADD_PET, payload: data });

export const getPets = (data) => (dispatch) => dispatch({ type: GET_PETS });
