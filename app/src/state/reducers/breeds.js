import { GET_BREEDS, GET_RANDOM_IMAGE_BY_BREED } from '../actions/types';
import { formatBreeds } from '../../utils';

const initialState = {
  breeds: [],
  breedImage: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_BREEDS:
      return {
        ...state,
        breeds: formatBreeds(action.payload.message)
      }
    case GET_RANDOM_IMAGE_BY_BREED:
      return {
        ...state,
        breedImage: action.payload
      }
    default:
      return state;
  }
}
