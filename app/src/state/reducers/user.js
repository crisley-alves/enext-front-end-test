import { ADD_PET, GET_PETS } from '../actions/types';

const initialState = {
  pets: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_PETS:
      const localPets = JSON.parse(localStorage.getItem('userPets')) || [];
      return {
        ...state,
        pets: localPets
      }
    case ADD_PET:
      const userPets = JSON.parse(localStorage.getItem('userPets')) || [];
      const nextId = userPets.length + 1;
      userPets.push({ id: nextId, ...action.payload });
      localStorage.setItem('userPets', JSON.stringify(userPets));
    default:
      return state;
  }
}
