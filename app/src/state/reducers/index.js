import { combineReducers } from 'redux';

import breeds from './breeds';
import user from './user';

export default combineReducers({ breeds, user });