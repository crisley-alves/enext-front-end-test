export const COLORS = [
  { label: 'Green', value: 'green' },
  { label: 'Red', value: 'red' },
  { label: 'Purple', value: 'purple' },
  { label: 'Black', value: 'black' },
  { label: 'pink', value: 'pink' },
];

export const FONTS = [
  { label: 'Lato', value: 'lato' },
  { label: 'Roboto', value: 'roboto' },
  { label: 'Montserrat', value: 'montserrat' },
  { label: 'Raleway', value: 'raleway' },
  { label: 'Oswald', value: 'oswald' },
];
