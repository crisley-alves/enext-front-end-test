import styled from 'styled-components';

export const Container = styled.div.attrs({
  className: 'HomePageContainer'
})`
  display: flex;
  flex-direction: column;
  background-color: #fff;
  padding: 15px;

  @media only screen and (min-width: 600px){
    flex-direction: row;
    width: 800px;
    margin: 15px auto;

    .FormComponent {
      width: 70%;
    }

    .PetContainer {
      width: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
    }
  }
`

export const Form = styled.div.attrs({
  className: 'FormComponent'
})`
  .MessageComponent {
    text-align: center;
  }
`;

export const InputGroup = styled.div`
  display: block;
  margin: 15px 0;
`;

export const PetsContainer = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;

  .CardContainer {
    margin: 15px;
    opacity: 0.7;

    &:hover {
      opacity: 1;
    }
  }

  @media only screen and (min-width: 600px){
    align-items: center;
    flex-direction: row;
    flex-wrap: wrap;
    flex-flow: row-wrap;
    align-content: flex-end;
  }
`;

export const PetContainer = styled.div.attrs({
  className: 'PetContainer'
})`
  margin: 25px auto;
`;