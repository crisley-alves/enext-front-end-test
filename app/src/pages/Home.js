import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import store from '../state/store';
import { getBreeds, getImageByBreed } from '../state/actions/breeds'
import { addPet, getPets } from '../state/actions/user'
import { getCurrentDate } from '../utils';

import Button from '../components/button';
import InputText from '../components/input-text';
import Label from '../components/label';
import Select from '../components/select';
import Card from '../components/card';
import Message from '../components/message';

import { COLORS, FONTS } from './data';
import { Container, Form, InputGroup, PetsContainer, PetContainer } from './styles';

const HomePage = (props) => {
  const [formData, setFormData] = useState({});
  const [breeds, setBreeds] = useState([]);

  useEffect(() => {
    store.dispatch(getBreeds());
    props.getPets();
  }, [])

  useEffect(() => {
    setBreeds(props.breeds)
  }, [props.breeds])

  useEffect(() => {
    if (formData.breed) {
      store.dispatch(getImageByBreed(formData.breed));
    }
    setFormData({ ...formData, breedImage: props.breedImage });
  }, [formData.breed])

  useEffect(() => {
    if (props.breedImage) {
      setFormData({ ...formData, breedImage: props.breedImage.message });
    }
  }, [props.breedImage])

  const onChangeValue = (e) => {
    setFormData({ ...formData, success: false, [e.target.name]: e.target.value });
  }

  const onSubmitForm = (e) => {
    props.addPet({ ...formData, date: getCurrentDate() });
    props.getPets(formData);
    setFormData({
      success: true,
      name: '',
      breed: '',
      font: '',
      color: ''
    });
    e.preventDefault();
  }

  const isFormValid = !!formData.name && formData.breed && formData.font && formData.color;

  return (
    <>
      <Container>
        <Form>
          <InputGroup>
            <Label
              name="name"
              label="Pet Name"
            />
            <InputText
              name="name"
              id="name"
              placeholder="Pet name"
              maxLength={20}
              value={formData.name}
              value={formData.name}
              onChange={onChangeValue}
            />
          </InputGroup>
          <InputGroup>
            <Label
              name="breed"
              label="Pet Breed"
            />
            <Select
              name="breed"
              id="breed"
              value={formData.breed}
              onChange={onChangeValue}
              options={breeds}
            />
          </InputGroup>
          <InputGroup>
            <Label
              name="font"
              label="Font"
            />
            <Select
              name="font"
              id="font"
              value={formData.font}
              onChange={onChangeValue}
              options={FONTS}
            />
          </InputGroup>
          <InputGroup>
            <Label
              name="color"
              label="Color"
            />
            <Select
              name="color"
              id="color"
              value={formData.color}
              onChange={onChangeValue}
              options={COLORS}
            />
          </InputGroup>

          <Button
            onClick={onSubmitForm}
            type="Submit"
            disabled={!isFormValid}
          />

          {formData.success && <Message />}
        </Form>
        <PetContainer>
          <Card
            color={formData.color}
            font={formData.font}
            name={formData.name}
            img={formData.breedImage}
          />
        </PetContainer>
      </Container>
      <PetsContainer>
        {props.pets.map(pet =>
          <Card
            color={pet.color}
            font={pet.font}
            name={pet.name}
            img={pet.breedImage}
            date={pet.date}
          />
        )}
      </PetsContainer>
    </>
  )
}

const mapStateToProps = state => ({
  breeds: state.breeds.breeds,
  breedImage: state.breeds.breedImage,
  pets: state.user.pets
});

export default connect(mapStateToProps, { addPet, getPets })(HomePage);