export const theme = {
  fonts: {
    lato: 'Lato, sans-serif;',
    roboto: 'Roboto, sans-serif;',
    montserrat: 'Montserrat, sans-serif',
    raleway: 'Raleway, sans-serif',
    oswald: 'Oswald, sans-serif',
  },
  colors: {
    green: 'green',
    red: 'red',
    purple: 'purple',
    black: 'black',
    pink: 'pink'
  }
};
