import styled from 'styled-components';

export const Label = styled.label`
  display: block;
  font-size: 12px;
  font-weight: 500;
  margin-bottom: 5px;

  span {
    color: red;
  }
`;