import React from 'react';
import { Label } from './styles';

const LabelComponent = ({
  name = '',
  label = ''
}) => <Label htmlFor={name}>{label} <span>*</span></Label>

export default LabelComponent;