import React from 'react';

import { Button } from './styles'

const ButtonComponent = ({
  value = 'Save',
  action,
  disabled = true,
  ...props
}) => <Button disabled={disabled} {...props}>{value}</Button>

export default ButtonComponent;