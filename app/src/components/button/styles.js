import styled from 'styled-components';

export const Button = styled.button.attrs({
  className: 'ButtonComponent'
})`
  width: 100%;
  background-color: #0980FF;
  color: #fff;
  border: 1px solid #e6e6e6;
  padding: 10px 15px;
  transition: 0.3s;

  &:disabled {
    background-color: #e6e6e6;
    color: #000;

    &:hover {
      background-color: #e6e6e6;
      color: #000;
    }
  }

  &:hover {
    background-color: #fff;
    color: #0980FF;
    cursor: pointer;
    transition: 0.3s;
  }
`;