import styled from 'styled-components';

export const Container = styled.div`
  display: 'flex';
  flex-direction: 'column';
`;
export const InputText = styled.input.attrs({
  className: 'TextInputComponent'
})`
  border: 1px solid rgba(0,0,0,0.2);
  color: rgba(0,0,0, 0.8);
  padding: 10px;
`;
