import React from 'react';
import { Container, InputText } from './styles';

const InputTextComponent = ({
  id = '',
  name = '',
  label = '',
  placeholder = '',
  ...props
}) => {
  return (
    <Container>
      <InputText type="text" name={name} placeholder={placeholder} {...props} />
    </Container>
  )
}

export default InputTextComponent;
