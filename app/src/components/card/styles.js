import styled from 'styled-components';

export const Container = styled.div.attrs({
  className: 'CardContainer'
})`
  width: 220px;
  height: 220px;
  position: relative;
  font-family: ${({ font, theme }) => theme.fonts[font]};

  @media only screen and (min-width: 600px){
    width: 220px;
    min-height: 300px;
    position: relative;
  }
`;

export const Text = styled.span`
  position: absolute;
  bottom: 5%;
  left: 5%;
  width: 90%;
  background: white;
  border-radius: 15px;
  text-align: center;
  padding: 10px;
  font-size: 12px;
  opacity: 1;
  color: green;
  z-index: 2;
  color: ${({ color, theme }) => theme.colors[color]}
`;

export const Image = styled.img`
  background-color: #e6e6e6;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1;
`;

export const Date = styled.span`
background: white;
  color: black;
  font-size: 12px;
  position: absolute;
  top: 3%;
  right: 3%;
  width: 70%;
  padding: 5px;
  text-align: center;
  opacity: 1;
  z-index: 2;
`;