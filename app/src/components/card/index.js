import React from 'react';

import { Container, Text, Image, Date } from './styles';

const Card = ({
  name = '',
  img = null,
  color = '',
  font = '',
  date = ''
}) =>
  <Container font={font}>
    <Text color={color}>{name}</Text>
    <Image src={img} />
    {date && <Date>{date}</Date>}
  </Container>

export default Card;