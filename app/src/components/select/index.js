import React from 'react';
import { Select, Option } from './styles';

const SelectComponent = ({
  id = '',
  name = '',
  value = '',
  options = [],
  ...props
}) => {
  return (
    <Select name={name} id={id} value={value} {...props}>
      <Option value="">Select one</Option>
      {options.map(
        option => <Option key={option.value} value={option.value} >{option.label}</Option>
      )}
    </Select>
  )
}

export default SelectComponent;
