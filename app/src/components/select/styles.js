import styled from 'styled-components';

export const Select = styled.select.attrs({
  className: 'SelectComponent'
})`
  width: 100%;
  border: 1px solid rgba(0,0,0,0.2);
  color: rgba(0,0,0,0.8);
  padding: 10px;
`

export const Option = styled.option.attrs({
  className: 'SelectOptionComponent'
})`
  text-transform: capitalize;
`