import styled from 'styled-components';

export const Text = styled.p.attrs({
  className: 'MessageComponent'
})`
  color: green;
  font-size: 14px;
  margin-top: 10px;
`;