export const formatBreeds = (data) => {
  const breeds = Object.entries(data).map(breed => {
    const [breedName, regions] = breed;

    const regionBreed = regions.reduce((acc, currentValue) => ({
      value: `${breedName}/${currentValue}`,
      label: `${breedName} ${currentValue}`,
    }), {})

    return {
      value: breedName,
      label: breedName,
      ...regionBreed
    }
  })

  return breeds;
}

export const getCurrentDate = () => {
  const today = new Date();
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  const yyyy = today.getFullYear();
  const hour = today.getHours();
  const minutes = today.getMinutes();
  const seconds = today.getSeconds();

  return `${mm}/${dd}/${yyyy}: ${hour}:${minutes}:${seconds}`;
}